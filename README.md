# For exam purpose only

## Tools that use
- Node.js
- Basic knowledge in Node.js’ Express framework
- RESTful APIs design.
- Docker
- Kubernetes

### Steps to make it work
---
# Created Restful API that can be deployed in Docker and Kubernetes

## Execution:

To see our deployment in that dashboard, execute the command below in your terminal.
`minikube dashboard`

Accessing the application
`minikube start service: rest-test-service` 

